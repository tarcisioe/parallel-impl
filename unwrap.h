#ifndef UNWRAP_H
#define UNWRAP_H

#include "types.h"
#include "matrix.h"

namespace phase {

auto find_neighbors(std::size_t x, std::size_t y);
auto find_neighbors(Position p);
short unwrap(short reference, short phase);
matrix::Matrix<byte> spatial_modulation(const matrix::Matrix<byte> &source);
matrix::Matrix<short> phase_unwrap(const matrix::Matrix<byte> &wrapped,
                  matrix::Matrix<byte> modulation,
                  std::size_t start_x, std::size_t start_y,
                  short f0_start);
matrix::Matrix<byte> rescale(const matrix::Matrix<short> &source);

}

#endif
