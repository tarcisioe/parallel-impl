#ifndef IMAGE_TIFF_H
#define IMAGE_TIFF_H

#include <string>
#include <vector>

#include <tiffio.h>

namespace image {
using TiffHandler = ::TIFF;

class TIFF {
public:
    ~TIFF()
    {
        TIFFClose(tiff);
        _TIFFfree(raster);
    }

    void flush()
    {
        TIFFFlush(tiff);
    }

    static TIFF open(const std::string &filename, const std::string &mode);

    auto width() const
    {
        return width_;
    }

    auto height() const
    {
        return height_;
    }

    std::pair<std::uint32_t, std::uint32_t> geometry() const
    {
        return {width_, height_};
    }

    std::uint32_t operator()(std::uint32_t x, std::uint32_t y) const
    {
        return raster[y*width_ + x];
    }

private:
    TiffHandler *tiff{nullptr};
    std::uint32_t width_{0u}, height_{0u};
    std::uint32_t *raster;

    TIFF() = default;
};

}

#endif
