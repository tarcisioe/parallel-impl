#ifndef UNWRAP_IMAGE_H
#define UNWRAP_IMAGE_H

#include "matrix.h"

namespace image {

matrix::Matrix<phase::byte> from_tiff(const image::TIFF &image);
void save_ppm(const matrix::Matrix<phase::byte> &bytes, const std::string &filename);

}

#endif
