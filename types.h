#ifndef UNWRAP_TYPES_H
#define UNWRAP_TYPES_H

#include <tuple>
#include <cstdint>

namespace phase {

using byte = std::uint8_t;
using Position = std::pair<size_t, size_t>;

}

#endif
