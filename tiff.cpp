#include "tiff.h"

namespace image {

TIFF TIFF::open(const std::string &filename, const std::string &mode)
{
    TIFF t;

    t.tiff = TIFFOpen(filename.c_str(), mode.c_str());

    std::uint32_t width, height;

    TIFFGetField(t.tiff, TIFFTAG_IMAGEWIDTH, &width);
    TIFFGetField(t.tiff, TIFFTAG_IMAGELENGTH, &height);

    t.width_ = width;
    t.height_ = height;

    t.raster = static_cast<uint32_t*>(_TIFFmalloc(width * height * sizeof(uint32_t)));

    if (t.raster) {
        TIFFReadRGBAImage(t.tiff, t.width_, t.height_, t.raster, 0);
    }

    return t;
}

}
