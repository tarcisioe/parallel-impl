#ifndef CUDA_PHASE_UNWRAPPING_ADAPTER_H
#define CUDA_PHASE_UNWRAPPING_ADAPTER_H

#include "matrix.h"

namespace phase {
namespace cuda {


using byte = unsigned char;
template <typename T>
using Matrix = matrix::Matrix<T>;
Matrix<byte> spatial_modulation(const Matrix<byte> &source);

}
}

#endif
