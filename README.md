Parallel Phase Unwrapping
=========================

This is an implementation of a phase unwrapping algorithm and its
requirements (modulation and rescaling) for the course INE410127
in UFSC's postgraduate program in Computer Science.

How to run
----------

Use the `execute.py` script to compile and run the code.

Options are:

* `--unwrap` to select the unwrapping strategy: `workers`, `default` or
  `batched`
* `--modulation` to select the modulation strategy: `CPU` or `GPU`
* `--compileonly` to just compile and not execute tests

If `matplotlib` is found, plots will be generated. Otherwise, collected
data will just be printed.
