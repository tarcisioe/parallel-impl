#include "image.h"

template <typename T>
using Matrix = matrix::Matrix<T>;

Matrix<phase::byte> image::from_tiff(const image::TIFF &image)
{
    auto width = image.width();
    auto height = image.height();

    auto bytes = Matrix<phase::byte>{width, height};

    #pragma omp parallel for
    for (auto y = 0; y < height; ++y) {
        auto opposite = height - 1 - y;
        for (auto x = 0u; x < width; ++x) {
            bytes(y, x) = (image(x, opposite) & 0xFF);
        }
    }

    return bytes;
}

void image::save_ppm(const Matrix<phase::byte> &bytes,
                      const std::string &filename)
{
    std::ofstream f{filename};
    auto width = bytes.width();
    auto height = bytes.height();

    f << "P2 " << width << ' ' << height << ' ' << 255 << '\n';

    for (auto y = 0u; y < height; ++y) {
        for (auto x = 0u; x < width; ++x) {
            auto colour = int{bytes(y, x)};
            f << colour << ' ';
        }
        f << '\n';
    }
}

