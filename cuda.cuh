#ifndef CUDA_PHASE_UNWRAPPING_H
#define CUDA_PHASE_UNWRAPPING_H

#include "types.h"

using phase::byte;

namespace phase {
namespace cuda {

void spatial_modulation_impl(const byte* source, byte* modulated, std::size_t width, std::size_t height);

}
}

#endif
