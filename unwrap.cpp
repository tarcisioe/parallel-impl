#include "unwrap.h"

#include <cmath>
#include <chrono>
#include <iostream>
#include <queue>

#ifdef _OPENMP
#include <omp.h>
#endif

#ifdef WORKERS_UNWRAP
#include <condition_variable>
#endif

using phase::byte;
using phase::Position;
using Data = std::pair<Position, short>;
using namespace std::chrono;

template <typename T>
using Matrix = matrix::Matrix<T>;

namespace {

template <typename T>
auto pop(std::queue<T>&q)
{
    auto k = q.front();
    q.pop();
    return k;
}

constexpr auto V1 = 2.0 * M_PI/256.0;
constexpr auto V2 = 255.0/9.0;

template <typename F>
auto lookup_table(F f)
{
    auto lut = std::array<double, 256>{};

    for (auto x = 0u; x < 256; ++x) {
        lut[x] = f(x);
    }

    return lut;
}

const auto LUT_SIN = lookup_table([](auto x) {
    return V2 * sin(x * V1);
});

const auto LUT_COS = lookup_table([](auto x) {
    return V2 * cos(x * V1);
});

}

auto phase::find_neighbors(std::size_t x, std::size_t y)
{
    auto neighbors = std::array<Position, 8>{
        {
            {x+1, y},
            {x, y-1},
            {x-1, y},
            {x, y+1},
            {x+1, y-1},
            {x-1, y-1},
            {x-1, y+1},
            {x+1, y+1},
        }
    };

    return neighbors;
}

auto phase::find_neighbors(Position p) {
    auto i = 0u, j = 0u;
    std::tie(i, j) = p;
    return find_neighbors(i, j);
}

short phase::unwrap(short reference, short phase)
{
    return phase - short(round((phase - reference)/256.0) * 256);
}

Matrix<byte> phase::spatial_modulation(const Matrix<byte> &source)
{
    auto width = source.width();
    auto height = source.height();
    auto stride = width;

    auto modulated = Matrix<byte>{width, height};

    #pragma omp parallel for
    for (auto y = 1u; y < height-1; ++y) {
        for (auto x = 1u; x < width-1; ++x) {
            auto neighbors = find_neighbors(y, x);

            auto s = LUT_SIN[source(y, x)];
            auto c = LUT_COS[source(y, x)];

            for (auto n: neighbors) {
                s += LUT_SIN[source(n)];
                c += LUT_COS[source(n)];
            }

            if (s != 0 and c != 255) {
                auto aux = sqrt(s*s + c*c);
                if (aux > 255) {
                    aux = 255;
                }
                modulated(y, x) = byte(aux);
            } else {
                modulated(y, x) = 0;
            }
        }
    }

    return modulated;
}


#ifdef WORKERS_UNWRAP

void asundi_first_step_task(const Matrix<byte> &wrapped,
                            const Matrix<byte> &modulation,
                            Matrix<byte> &status,
                            Matrix<short> &unwrapped,
                            std::queue<Data> &q1,
                            std::queue<Data> &q2,
                            byte threshold,
                            std::size_t &waiting,
                            std::condition_variable &event,
                            std::mutex &m)
{
    using phase::unwrap;
    using phase::find_neighbors;
    while (true) {
        auto p = Position{};
        auto v = short {};

        std::unique_lock<std::mutex> lock{m};
        if (q1.empty()) {
            ++waiting;

            if (waiting >= omp_get_num_threads()) {
                event.notify_all();
                return;
            }

            while (true) {
                event.wait(lock);
                if (waiting >= omp_get_num_threads()) {
                    return;
                }
                if (not q1.empty()) {
                    break;
                }
            }

            --waiting;
        }

        std::tie(p, v) = pop(q1);
        lock.unlock();

        auto value = unwrap(v, wrapped(p));
        unwrapped(p) = value;
        status(p) = 5;

        auto neighbors = find_neighbors(p);

        lock.lock();
        for (auto n: neighbors) {
            if (status(n) == 0) {
                if (modulation(n) > threshold) {
                    status(n) = 1;
                    q1.push({n, value});
                } else {
                    status(n) = 2;
                    q2.push({n, value});
                }
            }
        }
        event.notify_one();
        lock.unlock();
    }
}

void asundi_first_step(const Matrix<byte> &wrapped,
                       const Matrix<byte> &modulation,
                       Matrix<byte> &status,
                       Matrix<short> &unwrapped,
                       std::queue<Data> &q1,
                       std::queue<Data> &q2,
                       byte threshold)
{
    auto waiting = std::size_t{};
    std::mutex m{};
    std::condition_variable event{};

#pragma omp parallel
    asundi_first_step_task(wrapped, modulation, status,
                           unwrapped, q1, q2, threshold,
                           waiting, event, m);
}

#endif


Matrix<short> phase::phase_unwrap(const Matrix<byte> &wrapped,
                  Matrix<byte> modulation,
                  std::size_t start_x, std::size_t start_y,
                  short f0_start)
{
    auto width = wrapped.width();
    auto height = wrapped.height();
    auto unwrapped = Matrix<short>{wrapped};
    auto status = Matrix<byte>{width, height};

#ifdef BATCHED_UNWRAP

    auto current = std::vector<Data>{};
    auto to_visit = std::vector<Data>{};
    auto q2 = std::queue<Data>{};

    current.push_back({{start_y, start_x}, 256*f0_start});

    auto threshold = 250;

    auto d1 = 0ms, d2 = 0ms, d3 = 0ms;

#pragma parallel for
    for (auto i = 0u; i < 25; ++i) {
        while (not current.empty()) {
            auto before = high_resolution_clock::now();
            for (auto i = 0u; i < current.size(); ++i) {
                auto p = Position{};
                auto v = short {};

                std::tie(p, v) = current[i];
                auto value = unwrap(v, wrapped(p));
                unwrapped(p) = value;
                status(p) = 5;
            }
            auto after = high_resolution_clock::now();

            d1 += duration_cast<milliseconds>(after - before);

            before = high_resolution_clock::now();
            for (auto i = 0u; i < current.size(); ++i) {
                auto p = Position{};
                auto v = short {};

                std::tie(p, v) = current[i];
                auto neighbors = find_neighbors(p);

                for (auto n: neighbors) {
                    if (status(n) == 0) {
                        if (modulation(n) > threshold) {
                            status(n) = 1;
                            to_visit.push_back({n, unwrapped(p)});
                        } else {
                            status(n) = 2;
                            q2.push({n, unwrapped(p)});
                        }
                    }
                }
            }

            current.clear();
            std::swap(current, to_visit);
            after = high_resolution_clock::now();
            d2 += duration_cast<milliseconds>(after - before);
        }

        auto q2size = q2.size();

        threshold -= 10;

        auto before = high_resolution_clock::now();
        for (auto i = 0u; i < q2size; ++i) {
            auto p = Position{};
            auto v = short {};
            std::tie(p, v) = pop(q2);

            if (modulation(p) > threshold) {
                status(p) = 1;
                current.push_back({p, v});
            } else {
                q2.push({p, v});
            }
        }
        auto after = high_resolution_clock::now();
        d3 += duration_cast<milliseconds>(after - before);
    }

    std::cout
        << "\"d1\": " << d1.count() << ", "
        << "\"d2\": " << d2.count() << ", "
        << "\"d3\": " << d3.count() << ", ";

#elif defined WORKERS_UNWRAP


    auto q1 = std::queue<Data>{};
    auto q2 = std::queue<Data>{};

    q1.push({{start_y, start_x}, 256*f0_start});

    auto threshold = 250;

    for (auto i = 0u; i < 25; ++i) {
        asundi_first_step(wrapped, modulation,
                          status, unwrapped, q1, q2, threshold);

        auto q2size = q2.size();

        threshold -= 10;

        for (auto i = 0u; i < q2size; ++i) {
            auto p = Position{};
            auto v = short {};
            std::tie(p, v) = pop(q2);

            if (modulation(p) > threshold) {
                status(p) = 1;
                q1.push({p, v});
            } else {
                q2.push({p, v});
            }
        }
    }

#else

    auto q1 = std::queue<Data>{};
    auto q2 = std::queue<Data>{};

    q1.push({{start_y, start_x}, 256*f0_start});

    auto threshold = 250;

    for (auto i = 0u; i < 25; ++i) {
        while (not q1.empty()) {
            auto p = Position{};
            auto v = short {};

            std::tie(p, v) = pop(q1);
            auto value = unwrap(v, wrapped(p));
            unwrapped(p) = value;
            status(p) = 5;

            auto neighbors = find_neighbors(p);

            for (auto n: neighbors) {
                if (status(n) == 0) {
                    if (modulation(n) > threshold) {
                        status(n) = 1;
                        q1.push({n, value});
                    } else {
                        status(n) = 2;
                        q2.push({n, value});
                    }
                }
            }
        }

        auto q2size = q2.size();

        threshold -= 10;

        for (auto i = 0u; i < q2size; ++i) {
            auto p = Position{};
            auto v = short {};
            std::tie(p, v) = pop(q2);

            if (modulation(p) > threshold) {
                status(p) = 1;
                q1.push({p, v});
            } else {
                q2.push({p, v});
            }
        }
    }


#endif

    return unwrapped;
}

Matrix<byte> phase::rescale(const Matrix<short> &source)
{
    auto width = source.width();
    auto height = source.height();

    auto rescaled = Matrix<byte>{width, height};

    auto min = source(0, 0);
    auto max = min;

    #pragma omp parallel for
    for (auto y = 0u; y < height; ++y) {
        for (auto x = 0u; x < width; ++x) {
            auto aux = source(y, x);
            if (aux < min) { min = aux; }
            if (aux > max) { max = aux; }
        }
    }

    auto k1 = 0.0, k2 = 0.0;

    if (max > min) {
        k1 = 255.0/(max-min);
        k2 = -255.0 * double(min)/(max-min);
    } else {
        k1 = 0.0;
        k2 = 127.0;
    }

    #pragma omp parallel for
    for (auto y = 0u; y < height; ++y) {
        for (auto x = 0u; x < width; ++x) {
            auto pixel = source(y, x);
            auto rescaled_pixel = short(k1 * pixel + k2);
            if (rescaled_pixel < 0) { rescaled_pixel = 0; }
            if (rescaled_pixel > 255) { rescaled_pixel = 255; }
            rescaled(y, x) = rescaled_pixel;
        }
    }

    return rescaled;
}

