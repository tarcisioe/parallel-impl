#include "cuda.cuh"

#include <chrono>
#include <iostream>

constexpr auto V1 = 2.0 * M_PI/256.0;
constexpr auto V2 = 255.0/9.0;

__global__ void mean(const byte* source, byte* modulated, unsigned width, unsigned height) {
    auto x = blockIdx.x * blockDim.x + threadIdx.x;
    auto y = blockIdx.y * blockDim.y + threadIdx.y;

    auto threadid = x + y*width;

    if (x > width-1 or y > height-1) {
        return;
    }

    if (x == 0 or y == 0 or x == width-1 or y == height-1) {
        modulated[threadid] = 0;
        return;
    }
    double s = 0., c = 0.;
    for (auto j = -1; j <= 1; ++j) {
        for (auto i = -1; i <= 1; ++i) {
            auto value = source[threadid + i + j*width];
            s += V2 * sin(value*V1);
            c += V2 * cos(value*V1);
        }
    }

    if (s != 0 and c != 255) {
        auto aux = sqrt(s*s + c*c);
        modulated[threadid] = min(byte(255), byte(aux));
    } else {
        modulated[threadid] = 0;
    }
}

void phase::cuda::spatial_modulation_impl(const byte *source, byte *modulated, std::size_t width, std::size_t height) {
    byte *d_source, *d_modulated;
    auto size = width*height*sizeof(byte);

    cudaMalloc((void**)&d_source, size);
    cudaMalloc((void**)&d_modulated, size);

    cudaMemcpy(d_source, source, size, cudaMemcpyHostToDevice);

    struct cudaDeviceProp props;
    cudaGetDeviceProperties(&props, 0);
    auto max = sqrt(props.maxThreadsPerBlock);

    auto before = std::chrono::high_resolution_clock::now();
    auto dim_tpb = dim3(32, 32, 1);
    auto dim_numblocks = dim3(1+width/dim_tpb.x, 1+height/dim_tpb.y);

    mean<<<dim_numblocks, dim_tpb>>>(d_source, d_modulated, width, height);

    auto err = cudaPeekAtLastError();
    auto after = std::chrono::high_resolution_clock::now();
    cudaDeviceSynchronize();
    cudaMemcpy(modulated, d_modulated, size, cudaMemcpyDeviceToHost);

    cudaFree(d_source);
    cudaFree(d_modulated);
}

