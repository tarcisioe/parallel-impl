#include "cuda_adapter.h"
#include "cuda.cuh"

using phase::byte;
using matrix::Matrix;

Matrix<byte> phase::cuda::spatial_modulation(const Matrix<byte> &source)
{
    auto width = source.width();
    auto height = source.height();

    auto modulated = Matrix<byte>{width, height};

    phase::cuda::spatial_modulation_impl(source.data(), modulated.data(),
            width, height);

    return modulated;
}
