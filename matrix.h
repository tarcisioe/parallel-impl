#ifndef MATRIX_H
#define MATRIX_H

#include <fstream>
#include <tuple>
#include <vector>
#include "tiff.h"
#include "types.h"

namespace matrix {

template <typename T>
class Matrix {
public:
    Matrix(std::size_t width, std::size_t height) :
        width_{width},
        height_{height},
        contents(width_*height_)
    {}

    Matrix(const Matrix &) = default;

    template <typename U>
    friend class Matrix;

    template <typename U>
    Matrix(const Matrix<U> &other) :
        width_{other.width_},
        height_{other.height_},
        contents{begin(other.contents), end(other.contents)}
    {}

    const T &operator()(std::size_t i, std::size_t j) const
    {
        return contents[i*width_ + j];
    }

    T &operator()(std::size_t i, std::size_t j)
    {
        return contents[i*width_ + j];
    }

    const T &operator()(phase::Position p) const
    {
        auto i = 0u, j = 0u;
        std::tie(i, j) = p;
        return contents[i*width_ + j];
    }

    T &operator()(phase::Position p)
    {
        auto i = 0u, j = 0u;
        std::tie(i, j) = p;
        return contents[i*width_ + j];

    }

    std::size_t width() const
    {
        return width_;
    }

    std::size_t height() const
    {
        return height_;
    }

    std::size_t size() const
    {
        return contents.size();
    }

    const T* data() const {
        return contents.data();
    }

    T* data() {
        return contents.data();
    }

private:
    const std::size_t width_, height_;
    std::vector<T> contents;
};

}

#endif
