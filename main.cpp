#include <chrono>
#include <iostream>

#ifdef _OPENMP
#include <omp.h>
#endif

#ifdef USE_CUDA
#include "cuda_adapter.h"
#endif

#include "tiff.h"
#include "matrix.h"
#include "unwrap.h"
#include "image.h"


int main(int argc, char **argv)
{
    using phase::byte;
    using phase::Position;
    using image::TIFF;
    namespace chrono = std::chrono;

    auto filename = std::string{argv[1]};
    auto sx = atoi(argv[2]);
    auto sy = atoi(argv[3]);
    auto nthreads = atoi(argv[4]);

#ifdef _OPENMP
    omp_set_dynamic(0);
    omp_set_num_threads(nthreads);
#endif

    auto tiff = TIFF::open(filename, "r");

    auto before = chrono::high_resolution_clock::now();
    auto bytes = image::from_tiff(tiff);
    auto after = chrono::high_resolution_clock::now();

    std::cout << "{";

    std::cout
        << "\"image_loading\": "
        << chrono::duration_cast<chrono::milliseconds>(after-before).count()
        << ", ";

    auto overall_before = chrono::high_resolution_clock::now();
    before = chrono::high_resolution_clock::now();
#ifdef USE_CUDA
    auto modulated = phase::cuda::spatial_modulation(bytes);
#else
    auto modulated = phase::spatial_modulation(bytes);
#endif
    after = chrono::high_resolution_clock::now();

    std::cout
        << "\"modulation\": "
        << chrono::duration_cast<chrono::milliseconds>(after-before).count()
        << ", ";

    before = chrono::high_resolution_clock::now();
    auto unwrapped = phase::phase_unwrap(bytes, modulated, sx, sy, 0);
    after = chrono::high_resolution_clock::now();

    std::cout
        << "\"unwrapping\": "
        << chrono::duration_cast<chrono::milliseconds>(after-before).count()
        << ", ";

    before = chrono::high_resolution_clock::now();
    auto rescaled = phase::rescale(unwrapped);
    after = chrono::high_resolution_clock::now();

    std::cout
        << "\"rescaling\": "
        << chrono::duration_cast<chrono::milliseconds>(after-before).count()
        << ", ";

    auto overall_after = chrono::high_resolution_clock::now();
    std::cout
        << "\"overall\": "
        << chrono::duration_cast<chrono::milliseconds>(overall_after-overall_before).count();

    std::cout << "}\n";

    image::save_ppm(rescaled, "output.pgm");
}
