#!/usr/bin/python

import json
import shlex
import argparse

from sys import argv
from pprint import pprint
from statistics import mean
from subprocess import Popen, PIPE

try:
    from matplotlib import pyplot as plt
except ImportError:
    MATPLOTLIB=False
else:
    MATPLOTLIB=True

from collections import defaultdict


SIZES = {'plate': '640x480', 'large': '800x5120', 'giant': '6400x4800'}


def compile(compiler, output, files, flags):
    files = ' '.join(files)
    flags = ' '.join(flags)
    command = '{} {} {} {}'.format(compiler, output, files, flags)
    arglist = shlex.split(command)
    print(' '.join(arglist))
    p = Popen(arglist, stdin=None, stdout=PIPE)
    output, error = p.communicate()
    return str(output, 'utf-8')


def run(image, sx, sy, nthreads):
    command = './unwrap {} {} {} {}'.format(image, sx, sy, nthreads)
    arglist = shlex.split(command)
    p = Popen(arglist, stdin=None, stdout=PIPE)
    output, error = p.communicate()
    return str(output, 'utf-8')


def average(dicts):
    keys = dicts[0].keys()

    result = {}

    for key in keys:
        result[key] = mean(d[key] for d in dicts)

    return result


def measure(with_segmented=False):
    data = defaultdict(lambda: defaultdict(lambda: defaultdict(list)))

    for image in ('plate', 'large', 'giant'):
        for nthreads in (1, 2, 4, 8, 16):
            for i in range(4):
                print('running for {} {} {}'.format(i, image, nthreads))
                output = run('{}.tif'.format(image), 320, 240, nthreads)
                stats = json.loads(output)
                for stat in stats:
                    data[stat][image][nthreads].append(stats[stat])

    for stat in data:
        data[stat] = dict(data[stat])
        for image in data[stat]:
            data[stat][image] = dict(data[stat][image])
            for nthreads in data[stat][image]:
                data[stat][image][nthreads] = mean(data[stat][image][nthreads])

    data = dict(data)
    pprint(data)

    if MATPLOTLIB:
        plot(data)

    if with_segmented:
        plot_segmented(data)


def plot(data):
    for stat, images in data.items():
        plot_stat(stat, images, '{}.png'.format(stat))


def plot_stat(stat, images, output):
    fig, ax = plt.subplots()
    for image, nthreads in images.items():
        points = [y for _, y in sorted(nthreads.items())]
        line, = ax.plot(points, label=SIZES[image])
        ax.legend()

    ax.set_xticks((0, 1, 2, 3, 4))
    ax.set_xticklabels((1, 2, 4, 8, 16))
    fig.tight_layout()
    fig.savefig(output)


def plot_segmented(data):
    fig, ax = plt.subplots()
    stats = ['d1', 'd2', 'd3']
    bla = [(x, data[x]['large']) for x in stats]
    for stat, nthreads in bla:
        points = [y for _, y in sorted(nthreads.items())]
        line, = ax.plot(points, label=stat)
        ax.legend()

    ax.set_xticks((0, 1, 2, 3, 4))
    ax.set_xticklabels((1, 2, 4, 8, 16))
    fig.tight_layout()
    fig.savefig('segmented.png')


UNWRAP_METHODS = {
    'workers': 'WORKERS_UNWRAP',
    'batched': 'BATCHED_UNWRAP',
    'default': 'DEFAULT_UNWRAP'
        }

if __name__ == '__main__':
    flags = ['-std=c++14', '-O2', '-ltiff', '-fopenmp']
    compiler = 'g++'
    files = ['main.cpp', 'unwrap.cpp', 'image.cpp', 'tiff.cpp']

    parser = argparse.ArgumentParser('execute.py')
    parser.add_argument('--modulation', action='store', default='CPU')
    parser.add_argument('--unwrap', action='store', default='default')
    parser.add_argument('--compileonly', action='store_true', default=False)

    args = parser.parse_args(argv[1:])
    if args.modulation == 'GPU':
        compiler = 'g++-5'
        compile('nvcc', '-dc cuda.cu', [],
                ['-lcudart', '-lcudadevrt', '-std=c++11'])
        compile('nvcc', '-o cuda_dlinked.o', ['cuda.o'], ['-dlink'])
        files.append('cuda.o')
        files.append('cuda_dlinked.o')
        files.append('cuda_adapter.cpp')
        flags.append(
                '-D USE_CUDA -O2 '
                '-lcudart -lcudadevrt -L/opt/cuda/lib64')
    flags.append('-D {}'.format(UNWRAP_METHODS[args.unwrap]))

    compile(compiler, "-o unwrap", files, flags)
    if (not args.compileonly):
        measure(args.unwrap == 'batched')
